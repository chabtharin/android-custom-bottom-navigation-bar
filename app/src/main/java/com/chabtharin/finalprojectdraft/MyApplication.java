package com.chabtharin.finalprojectdraft;

import android.app.Application;
import android.os.Build;

import com.google.android.material.color.DynamicColors;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // This is all you need.
        DynamicColors.applyToActivitiesIfAvailable(this);
    }
}
